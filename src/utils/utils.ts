import { Card, DynamicKey } from '../model.app';

export function getClassName(classNames: DynamicKey<boolean | any>): string {
  return Object.entries(classNames).filter(([, condition]) => Boolean(condition)).map(([name]) => name).join(' ');
}

const values: DynamicKey<number> = {
  ACE: 1,
  2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8, 9: 9, 10: 10,
  JACK: 12,
  QUEEN: 13,
  KING: 14,
};

export function getRandomIndex(len: number) {
  return Math.floor(Math.random() * len);
}

export function getRoundWinner(pile: Array<Card>): { roundWinner: number, sum: number; } {
  let roundWinner = 0;
  let max = 0;
  let sum = 0;

  pile.forEach(({ value }, index) => {
    const val = values[value];
    sum += val;
    if (val >= max) {
      roundWinner = index;
      max = val;
    }
  });
  return { roundWinner, sum };
}
