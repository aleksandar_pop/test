import axios from 'axios';
import { DynamicKey } from '../model.app';

const config = {
  baseUrl: 'https://deckofcardsapi.com/api/deck/'
};

axios.interceptors.response.use(
  res => res,
  err => (console.log('interceptor', err), Promise.reject(err)));


class Http {

  private http = axios.create({
    baseURL: config.baseUrl
  });

  get(url: string) {
    return this.http.get(url).then(res => res.data);
  }

  post(url: string, params: DynamicKey<any>) {
    return this.http.post(url, params);
  }

}

export default new Http();
