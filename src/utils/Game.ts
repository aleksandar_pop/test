import { BehaviorSubject, Subject } from 'rxjs';
import { Card, PlayerType } from '../model.app';
import http from './Http';
import { getRandomIndex, getRoundWinner } from './utils';

interface DeckType {
  deck_id: string;
  remaining: number;
}

const initNumberofCards = 10;
class Game {

  $players = new BehaviorSubject<Array<PlayerType>>([]);
  $pile = new BehaviorSubject<Array<Card>>([]);
  $winers = new Subject<Array<PlayerType>>();
  deck!: DeckType;

  set players(players: Array<PlayerType>) {
    this.$players.next(players);
  }

  get players() {
    return this.$players.getValue();
  }

  async newGame(numOfPlayers: number) {
    const players: Array<string> = ['me'];
    await http
      .get(`https://randomuser.me/api?results=${numOfPlayers - 1}&noinfo`)
      .then(res => res.results.forEach((user: any) => players.push(user.name.first)));

    this.players = players.map(name => ({ name, cards: [], score: 0 }));
    this.deck = await http.get(`new/shuffle/?deck_count=1`);
    this.dealCards(initNumberofCards);
  }

  async dealCards(amount: number) {
    const cards = await Promise.all(this.players.map(_ => http.get(`${this.deck?.deck_id}/draw/?count=${amount}`)));
    this.deck = { ...this.deck, remaining: cards.at(-1).remaining };
    this.players = this.players.map((player, index) => ({ ...player, cards: cards[index].cards }));
  }

  getWinner() {
    let highestScore = 0;
    const winers: Array<number> = [];

    this.players.forEach(({ score }, index) => {
      if (score === highestScore) {
        winers.push(index);
      }
      else if (score > highestScore) {
        winers.length = 0;
        highestScore = score;
        winers.push(index);
      }
    });

    this.$winers.next(winers.map(index => this.players[index]));
  }
  play(index: number, playerIndex = 0) {
    const players = [...this.players];
    const pile = [...this.$pile.getValue()];
    pile[playerIndex] = players[playerIndex].cards.splice(index, 1)[0];

    this.$pile.next(pile);
    this.$players.next(players);

    if (playerIndex < players.length - 1) {
      const nextPlayer = playerIndex + 1;
      setTimeout(() => {
        this.play(getRandomIndex(players[nextPlayer]?.cards.length), nextPlayer);
      }, 300);
    }
    else {
      setTimeout(() => {
        const { roundWinner, sum } = getRoundWinner(pile);
        players[roundWinner].score += sum;
        this.$pile.next([]);
        this.$players.next([...players]);
        if (!players[0].cards.length) {
          if (this.deck?.remaining) {
            const amount = Math.floor(this.deck?.remaining / this.players.length);
            amount > 3 ? this.dealCards(amount) : this.getWinner();
          }
          else {
            this.getWinner();
          }
        }
      }, 700);
    }
  }

}

export default new Game();
