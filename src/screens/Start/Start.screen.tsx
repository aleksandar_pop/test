import './start.screen.scss';

interface StartScreenProps {
  onChange: Function;
}
export default function StartScreen({ onChange }: StartScreenProps) {
  return (
    <div className='start-screen-wrapper'>
      <h1>Select number of players</h1>
      <button onClick={onChange.bind(null, 2)}>2 Players</button>
      <button onClick={onChange.bind(null, 3)}>3 Players</button>
      <button onClick={onChange.bind(null, 4)}>4 players</button>
    </div>
  );
}
