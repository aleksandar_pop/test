import { ReactElement, useEffect, useState } from 'react';
import { Subscription } from 'rxjs';
import PileComponent from '../../components/pile/Pile.component';
import Player from '../../components/player/Player.component';
import { PlayerType } from '../../model.app';
import Game from '../../utils/Game';
import { getClassName } from '../../utils/utils';
import './game.screen.scss';

interface GameProps {
  numOfPlayers: number;
}

export default function GameComponent({ numOfPlayers }: GameProps): ReactElement {
  const [players, setPlayers] = useState<Array<PlayerType>>();
  const [disabled, setDisabled] = useState<boolean>();

  useEffect(() => {
    Game.newGame(numOfPlayers);
    const playersSubs: Subscription = Game.$players.subscribe(setPlayers);
    const pileSubs: Subscription = Game.$pile.subscribe(pile => setDisabled(!!pile.length));

    return () => {
      playersSubs?.unsubscribe();
      pileSubs?.unsubscribe();
    };
  }, []);

  return (
    <div className={getClassName({ game: true, [`players-${numOfPlayers}`]: true })}>
      {players?.map((player, index) => (
        <div key={player.name.replace(/\s/g, '')} className={!index ? 'me' : `player player-${index}`}>
          <Player {...player} disabled={disabled} />
        </div>
      ))}
      <div className='pile'>
        <PileComponent numOfPlayers={numOfPlayers} />
      </div>
    </div>
  );
}
