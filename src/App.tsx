import { ReactElement, useEffect, useState } from 'react';
import { Subscription } from 'rxjs';
import './App.scss';
import Conditional from './components/conditional/Conditional.component';
import ModalComponent from './components/modal/Modal.component';
import GameComponent from './screens/game/Game.screen';
import StartScreen from './screens/Start/Start.screen';
import Game from './utils/Game';

export default function App(): ReactElement {
  const [numOfPlayers, setnumOfPlayers] = useState<number>(0);
  const [winnerText, setWinnerText] = useState<string>();

  function onQuit() {
    setnumOfPlayers(0);
    setWinnerText('');
  }

  useEffect(() => {
    const winersSubs: Subscription = Game.$winers.subscribe(players =>
      setWinnerText(`Winer is ${players.map(({ name }) => name).join(', ')} with score: ${players[0].score}`)
    );

    return () => {
      winersSubs?.unsubscribe();
    };
  }, []);

  return (
    <Conditional condition={!!numOfPlayers} otherwise={<StartScreen onChange={setnumOfPlayers} />}>
      <GameComponent numOfPlayers={numOfPlayers} />
      <ModalComponent onQuit={onQuit} modalText={winnerText} />
    </Conditional>
  );
}
