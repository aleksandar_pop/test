import { useEffect, useState } from 'react';
import { Subscription } from 'rxjs';
import { Card } from '../../model.app';
import Game from '../../utils/Game';
import { getClassName } from '../../utils/utils';
import './pile.scss';

export default function PileComponent({ numOfPlayers }: { numOfPlayers: number }) {
  const [cards, setCards] = useState<Array<Card>>([]);

  useEffect(() => {
    const subs: Subscription = Game.$pile.subscribe(cards => setCards(cards));
    return () => subs?.unsubscribe();
  }, []);

  return (
    <div className={getClassName({ 'pile-cards-wrapper': true, [`cards-${numOfPlayers}`]: true })}>
      {cards.map((card, index) => (
        <div key={card?.code || index} className={getClassName({ card: true, [`card-${index}`]: true })}>
          <img src={card?.image} alt={card?.code}></img>
        </div>
      ))}
    </div>
  );
}
