import { ReactElement } from 'react';

interface ConditionalProps {
  condition: boolean | Function;
  children: any;
  otherwise?: ReactElement;
}

export default function Conditional({ condition, otherwise, children }: ConditionalProps) {
  const _if = condition instanceof Function ? condition() : condition;
  return _if ? children : otherwise || null;
}
