import { useEffect } from 'react';
import { PlayerType } from '../../model.app';
import Game from '../../utils/Game';
import Cards from '../cards/Cards.component';
import './player.scss';

export default function Player({ name, cards, score, winner, disabled }: PlayerType) {
  function onChangeHendler(index: number) {
    Game.play(index);
  }

  useEffect(() => {}, []);
  return (
    <div>
      <h2>{name || 'N/A'}</h2>
      <h4>
        Score: {score}
        {winner && <span className='score-color'>'Winner'</span>}
      </h4>
      <Cards cards={cards} isVisible={name === 'me'} disabled={disabled} onChange={!disabled ? onChangeHendler : undefined} />
    </div>
  );
}
