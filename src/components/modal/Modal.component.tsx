import { useEffect, useState } from 'react';
import Conditional from '../conditional/Conditional.component';
import './modal.scss';

export default function ModalComponent({ onQuit, modalText }: any) {
  const [text, setText] = useState('');

  function onkKeyPress(event: KeyboardEvent) {
    if (event.key === 'Escape') {
      setText(text => (!text ? 'Are you sure you want to quit to the main menu' : ''));
    }
  }

  function onCancel() {
    setText('');
  }

  useEffect(() => {
    document.addEventListener('keydown', onkKeyPress);
    return () => document.removeEventListener('keydown', onkKeyPress);
  }, []);

  useEffect(() => {
    setText(modalText);
  }, [modalText]);

  return (
    <Conditional condition={!!text}>
      <div className='modal-backdrop'>
        <div className='modal'>
          <h1>{text}</h1>
          <div className='buttons'>
            <Conditional condition={!modalText}>
              <button onClick={onCancel}>Cancel</button>
            </Conditional>
            <button onClick={onQuit}>Ok</button>
          </div>
        </div>
      </div>
    </Conditional>
  );
}
