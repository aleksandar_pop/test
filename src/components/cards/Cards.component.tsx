import { ReactElement } from 'react';
import { Card } from '../../model.app';
import { getClassName } from '../../utils/utils';
import './cards.scss';

interface CardsProps {
  isVisible: boolean;
  cards: Array<Card>;
  onChange?: (arg: any) => void;
  disabled?: boolean;
}

export default function Cards({ cards, isVisible, onChange, disabled }: CardsProps): ReactElement {
  return (
    <div className={getClassName({ cards, isVisible })}>
      {cards.map((card, index) => (
        <div key={card.code} className={getClassName({ card: true, disabled })} style={!isVisible ? { transform: `translateX(calc(-75% * ${index}))` } : {}}>
          {isVisible && <img src={card.image} alt={card.code} onClick={onChange?.bind(null, index)}></img>}
        </div>
      ))}
    </div>
  );
}
