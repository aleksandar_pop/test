export interface DynamicKey<T> { [key: string]: T; }

export interface Card {
  code: string;
  image: string;
  images: { svg: string; png: string; };
  suit: string;
  value: string;
}

export interface PlayerType {
  name: string;
  cards: Array<Card>;
  score: number;
  className?: string;
  winner?: boolean;
  disabled?: boolean;
}
